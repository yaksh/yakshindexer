package edu.upenn.cis455.postProccessing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;

import edu.upenn.cis455.storage.DynamoStorage;
import edu.upenn.cis455.storage.S3Storage;

public class PostProccessing implements Runnable{
	
	private String key;
	private S3Storage s3;
//	private DynamoStorage dynamoDb;
	
	public PostProccessing(String key) {
		this.key = key;
		this.s3 = new S3Storage(StartProccessing.awsAccessKeyNick, StartProccessing.awsSecretKeyNick);
//		this.dynamoDb = new DynamoStorage(StartProccessing.awsAccessKeyJatin, StartProccessing.awsSecretKeyJatin);
	}

	@Override
	public void run() {
		try{
			//Download file
			File downloaded = File.createTempFile("index", ".txt");
			System.out.println(Thread.currentThread().getName() + "-Downloading file " + key);
			s3.download(StartProccessing.indexOutBucket, key, downloaded);
			
			System.out.println(Thread.currentThread().getName() + "-Proccessing file " + key);
			BTreeDB db = new BTreeDB();
			Long[] results = db.add(downloaded);
			
			System.out.println(Thread.currentThread().getName() + "-Done proccessing file " + key + " Keys Read: " + results[0] + " Keys Written: " + results[1]);
		}
		catch(Exception e){
			System.err.println(Thread.currentThread().getName() + "-Error proccessing file " + key);
		}
	}
}
