package edu.upenn.cis455.postProccessing;

import java.util.LinkedList;
import java.util.Queue;

public class CustomisedBlockingQueue<E> implements BlockingQueue<E>{
	
	private Queue<E> queue = new LinkedList<E>();
	
	//Enqueue method for the queue
	@Override
	public synchronized void enqueue(E e) {
		queue.add(e);
		notify();
	}
	
	//Dequeue method for the queue
	@Override
	public synchronized E dequeue() {
		E e = null;
		while(queue.isEmpty()){
			try {
				wait();
			}
			catch(InterruptedException e1) {
				//Add logging statements
				return e;
			}
		}
		e = queue.remove();
		return e;
	}

}
