package edu.upenn.cis455.postProccessing;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import edu.upenn.cis455.storage.S3Storage;


public class StartProccessing {
	
	public static String awsAccessKeyNick;
	public static String awsSecretKeyNick;
	
	public static String awsAccessKeyJatin;
	public static String awsSecretKeyJatin;
	
	public static String indexOutBucket = "yaksh-indexer";
	public static String indexOutFolder = "out-4";
	
	public static void main(String[] args) {
		StartProccessing.awsAccessKeyNick = args[0];
		StartProccessing.awsSecretKeyNick = args[1];
		
		StartProccessing.awsAccessKeyJatin = args[2];
		StartProccessing.awsSecretKeyJatin = args[3];
		
//		ThreadPoolManager threadPoolManager = new ThreadPoolManager(5);
		ExecutorService threadPoolManager = Executors.newFixedThreadPool(4);
		S3Storage s3 = new S3Storage(StartProccessing.awsAccessKeyNick, StartProccessing.awsSecretKeyNick);
    	ArrayList<String> outKeys = s3.getFilesInFolder(StartProccessing.indexOutBucket, StartProccessing.indexOutFolder);
    	
    	int count = 0;
    	for(String key: outKeys){
    		count++;
    		PostProccessing postProccessing = new PostProccessing(key);
    		threadPoolManager.submit(postProccessing);
//    		if(count == 2)
//    			break;
    	}
    	
    	while(!threadPoolManager.isTerminated());
    	
    	System.exit(0);
		
	}
}
