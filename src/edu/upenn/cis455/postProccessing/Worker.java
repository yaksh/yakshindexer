package edu.upenn.cis455.postProccessing;

/**
 * Worker class to keep polling the shared queue for any pending requests to be served
 * @author nbehera
 *
 */
public class Worker implements Runnable{
	
	private CustomisedBlockingQueue<Runnable> queue;
	private String threadId;
	private volatile Boolean isRunning;
	public static String request;
	
	public Worker(CustomisedBlockingQueue<Runnable> queue, String threadId) {
		this.queue = queue;
		this.threadId = threadId;
	}
	
	@Override
	public void run() {
		isRunning = true;
		while(isRunning) {
			Runnable r = queue.dequeue();
			//Started work for thread
			if(r != null) {
				r.run();
			}
			//Finished work for the thread
		}
	}
	
	public void stop() {
		isRunning = false;
	}
}