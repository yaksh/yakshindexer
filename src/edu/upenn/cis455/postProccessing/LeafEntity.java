/**
 * 
 */
package edu.upenn.cis455.postProccessing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMarshalling;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import edu.upenn.cis455.postProccessing.CacheUrl.CacheUrlConverter;

/**
 * @author Jatin Sharma
 *
 */

@DynamoDBTable(tableName = "yaksh-index")
public class LeafEntity implements Serializable{
	
	private String word;
//	private String filePath;
	private ArrayList<CacheUrl> cacheUrlList = new ArrayList<CacheUrl>();
	private int IDF;
	
	public LeafEntity(String word, String filePath, ArrayList<CacheUrl> cacheUrlList){
		this.word =  word;
//		this.filePath = filePath;
		// sort the list on PageRank 
		Collections.sort(cacheUrlList);
		this.cacheUrlList = cacheUrlList;
		this.IDF = cacheUrlList.size();
	}

	@DynamoDBHashKey(attributeName = "word")
	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

//	@DynamoDBAttribute(attributeName = "filePath")
//	public String getFilePath() {
//		return filePath;
//	}
//
//	public void setFilePath(String filePath) {
//		this.filePath = filePath;
//	}

	@DynamoDBMarshalling(marshallerClass = CacheUrlConverter.class)
	@DynamoDBAttribute(attributeName = "urlList")
	public ArrayList<CacheUrl> getCacheUrlList() {
		return cacheUrlList;
	}

	public void setCacheUrlList(ArrayList<CacheUrl> cacheUrlList) {
		this.cacheUrlList = cacheUrlList;
	}

	@DynamoDBAttribute(attributeName = "idf")
	public int getIDF() {
		return IDF;
	}

	public void setIDF(int iDF) {
		IDF = iDF;
	}
}

