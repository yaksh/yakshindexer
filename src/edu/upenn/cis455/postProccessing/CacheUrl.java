package edu.upenn.cis455.postProccessing;

import java.io.Serializable;
import java.util.ArrayList;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMarshaller;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.Persistent;

/**
 * @author Jatin Sharma
*/

public class CacheUrl implements Serializable, Comparable<CacheUrl>{
	
	private String url;
	private double PR;
	private int TF;
	private ArrayList<Integer> termLocations;
	
	public CacheUrl(){
		this.TF = 0;
	}
	
	public CacheUrl(String url, double PR, int TF, ArrayList<Integer> termLocations){
		this.url = url;
		this.PR = PR;
		this.TF = TF;
		this.termLocations = termLocations;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CacheUrl that) {
		//returns 1 if "this" object is less than "that" object
	    //returns 0 if they are equal
	    //returns -1 if "this" object is greater than "that" object
		if(this.PR < that.PR)
			return 1;
		else if(this.PR > that.PR)
			return -1;
		else
			return 0;
	}
	
	@Override
	public String toString(){
		return url;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the pR
	 */
	public double getPR() {
		return PR;
	}

	/**
	 * @param pR
	 *            the pR to set
	 */
	public void setPR(double pR) {
		PR = pR;
	}

	/**
	 * @return the tF
	 */
	public int getTF() {
		return TF;
	}

	/**
	 * @param tF
	 *            the tF to set
	 */
	public void setTF(int tF) {
		TF = tF;
	}

	/**
	 * @return the termLocations
	 */
	public ArrayList<Integer> getTermLocations() {
		return termLocations;
	}

	/**
	 * @param termLocations
	 *            the termLocations to set
	 */
	public void setTermLocations(ArrayList<Integer> termLocations) {
		this.termLocations = termLocations;
	}
	
	static public class CacheUrlConverter implements DynamoDBMarshaller<ArrayList<CacheUrl>> {

		@Override
		public String marshall(ArrayList<CacheUrl> arg0) {
			String marshalled = "";
			ArrayList<CacheUrl> cachedUrl = arg0;
			try{
				for(CacheUrl c : cachedUrl){
					String termLoc = "";
					if(cachedUrl != null){
						marshalled += String.format("%s || %s || %s ^^^", 
								c.getUrl(),c.getPR(),c.getTF());
					}
				}
				marshalled = marshalled.substring(0, marshalled.length() - 3);
			}
			catch(Exception e){
				
			}
			return marshalled;
		}

		@Override
		public ArrayList<CacheUrl> unmarshall(Class<ArrayList<CacheUrl>> arg0,
				String arg1) {
			ArrayList<CacheUrl> cacheUrls = new ArrayList<>();
			try{
				if(arg1 != null && arg1.length() != 0){
					String[] urls = arg1.split("^^^");
					for(String url : urls){
						CacheUrl cacheUrl = new CacheUrl();
						String[] data = arg1.split("\\|\\|");
						if(data.length == 3){
							cacheUrl.setUrl(data[0].trim());
							cacheUrl.setPR(Double.parseDouble(data[1].trim()));
							cacheUrl.setTF(Integer.parseInt(data[2].trim()));
						}
						cacheUrls.add(cacheUrl);
					}
				}
			}
			catch(Exception e){
				
			}
			return cacheUrls;
		}
	}	
}


