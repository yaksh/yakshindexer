package edu.upenn.cis455.postProccessing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import edu.upenn.cis455.job.IndexerMain;
import edu.upenn.cis455.storage.DynamoStorage;

/**
 * @author cis455
 *
 */
public class BTreeDB {
	
	private String directoryPath;
	private DynamoStorage dynamo;
	
	public BTreeDB() {
		this.dynamo = new DynamoStorage(IndexerMain.awsAccessKeyJatin, IndexerMain.awsSecretKeyJatin);
	}


	public void add(String filePath) {
		File fin = new File(filePath);
		add(fin);
	}
	
	public void addLine(String line){
		try {
			String word;
			String cacheUrlString;
			String[] keyVal;
			String[] urlWordFreqArray;
			String[] urlWordFreq;
			String url;
			String[] wordLocationsStrings;
			ArrayList<Integer> wordLocations = new ArrayList<Integer>();
			ArrayList<CacheUrl> cacheUrlList;
			StringBuffer UrlBuffer;
			LeafEntity leaf;
			ArrayList<String> urls = new ArrayList<>();
			String[] uw;
			ArrayList<LeafEntity> words = new ArrayList<>();
			try{
				if(line.contains("\t")){
					keyVal = line.split("\t");
					word = keyVal[0];
					cacheUrlString = keyVal[1];
//					System.out.println("("+word+", "+cacheUrlString+")");
					
					// create a leaf and add to BTree
					cacheUrlList = new ArrayList<CacheUrl>();
					
					// what if only one URL is there. No pipes. [e.g. word <tab> url1{{0,4,5]
					UrlBuffer = new StringBuffer();
					if(cacheUrlString.contains("||")){
						urlWordFreqArray = cacheUrlString.split("\\|\\|");
						if(urlWordFreqArray != null && urlWordFreqArray.length > 0){
							
							for(int i=0; i<urlWordFreqArray.length; i++){
								try{
									urlWordFreq = urlWordFreqArray[i].split("\\{\\{");
									if(urlWordFreq.length > 1){
										url = urlWordFreq[0];
										wordLocationsStrings = urlWordFreq[1].split(",");
										double urlPR = 0.15;
//										double r = dynamo.getRank(url);
//										if(r != -1){
//											urlPR = r;
//										}
//										
										UrlBuffer.append(url+"\t"+urlPR+"\n");
										cacheUrlList.add(new CacheUrl(
												url, 
												urlPR, 
												wordLocationsStrings.length, 
												wordLocations));
									}
								}
								catch(Exception e){}
							}
						}
					}
					
					leaf = new LeafEntity(word, word+".txt", cacheUrlList);
//					words.add(leaf);
					dynamo.storeWord(leaf);
				}
			}catch(Exception ex){
				System.err.println("Error parsing the word file");
//				ex.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Long[] add(File fin) {
		
		FileInputStream fis;
		BufferedReader br = null;
		Long[] counts = {(long) 0,(long) 0};
		long keysRead = 0;
		long keysWritten = 0;
		
		try {
			fis = new FileInputStream(fin);
			br = new BufferedReader(new InputStreamReader(fis));

			// word	<tab> url1;;0,3,5||url4;;20,31,57||url57;;0,3,5
			String line = null;
			String word;
			String cacheUrlString;
			String[] keyVal;
			String[] urlWordFreqArray;
			String[] urlWordFreq;
			String url;
			String[] wordLocationsStrings;
			ArrayList<Integer> wordLocations = new ArrayList<Integer>();
			ArrayList<CacheUrl> cacheUrlList;
			StringBuffer UrlBuffer;
			LeafEntity leaf;
			ArrayList<String> urls = new ArrayList<>();
			String[] uw;
			ArrayList<LeafEntity> words = new ArrayList<>();
			while((line = br.readLine()) != null){
				keysRead++;
				urls.clear();
				if(words.size() >= 15){
					dynamo.storeWords(words);
					words.clear();
				}
				// each line is one leaf
				// word	<tab> url1;;0,3,5||url4;;20,31,57||url57;;0,3,5

				try{
					if(line.contains("\t")){
						keyVal = line.split("\t");
						word = keyVal[0];
						cacheUrlString = keyVal[1];
//						System.out.println("("+word+", "+cacheUrlString+")");
						
						// create a leaf and add to BTree
						cacheUrlList = new ArrayList<CacheUrl>();
						
						// what if only one URL is there. No pipes. [e.g. word <tab> url1{{0,4,5]
						UrlBuffer = new StringBuffer();
						if(cacheUrlString.contains("||")){
							urlWordFreqArray = cacheUrlString.split("\\|\\|");
							if(urlWordFreqArray != null && urlWordFreqArray.length > 0){
								
								for(int i=0; i<urlWordFreqArray.length; i++){
									try{
										urlWordFreq = urlWordFreqArray[i].split("\\{\\{");
										if(urlWordFreq.length > 1){
											url = urlWordFreq[0];
											wordLocationsStrings = urlWordFreq[1].split(",");
//											System.out.println(urlWordFreq[1] + " " + wordLocationsStrings.length);
//											for (int k=0; k<wordLocationsStrings.length; k++){
//												try{
//													Integer loc = Integer.parseInt(wordLocationsStrings[k].trim());
//													wordLocations.add(loc);
//												}
//												catch(Exception e){}
//											}
											
											//save in BTree (PR- not known yet)
//											double urlPR = new Random().nextDouble()*10;
											double urlPR = 0.15;
//											double r = dynamo.getRank(url);
//											if(r != -1){
//												urlPR = r;
//											}
											
											UrlBuffer.append(url+"\t"+urlPR+"\n");
											cacheUrlList.add(new CacheUrl(
													url, 
													urlPR, 
													wordLocationsStrings.length, 
													wordLocations));
										}
									}
									catch(Exception e){}
								}
							}
						}
						
						leaf = new LeafEntity(word, word+".txt", cacheUrlList);
						words.add(leaf);
						keysWritten++;
					}
				}catch(Exception ex){
					System.err.println("Error parsing the word file");
//					ex.printStackTrace();
				}
				
			}
			if(words.size() > 0){
				dynamo.storeWords(words);
				words.clear();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			counts[0] = keysRead;
			counts[1] = keysWritten;
		}
		return counts;
	}
	
//	public ArrayList<CacheUrl> get(String word){
//		
//	}
	

	public static void main(String[] args) {

//		// one time
//		BTreeDB bTree = new BTreeDB("/home/cis455/workspace/BTreeStore");
//		bTree.add("/home/cis455/workspace/IndexedStore/test.txt");
//		
//		bTree.BTreeMap.open();
//		LeafEntity appleLeaf = bTree.BTreeMap.get("apple");
//		System.out.println(appleLeaf.getFilePath());
//		System.out.println(appleLeaf.getCacheUrlList().size());
//		for(CacheUrl url : appleLeaf.getCacheUrlList()){
//			System.out.println(url.toString()+"\t"+url.getPR()+"\t"+url.getTF());
//		}
//		//don't forget to close the DB once you're done
//		bTree.BTreeMap.close();	
		
	}
}
