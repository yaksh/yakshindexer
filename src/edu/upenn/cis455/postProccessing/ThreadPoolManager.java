package edu.upenn.cis455.postProccessing;

import java.util.HashMap;

public class ThreadPoolManager {
	
	//Maximum number of threads in the thread pool
	private final int MAX_THREADS;
	private CustomisedBlockingQueue<Runnable> queue = new CustomisedBlockingQueue<Runnable>();
	private Worker[] workers;
	private Thread[] threads;
	public HashMap<Long, String> threadState;
	
	//Constructor
	/**
	 * @param maxThreads
	 */
	public ThreadPoolManager(int maxThreads) {
		this.MAX_THREADS = maxThreads;
		workers = new Worker[MAX_THREADS];
		threads = new Thread[MAX_THREADS];
		initialiseThreads();
		threadState = new HashMap<Long, String>();
	}
	
	/**
	 * Initializing all the threads
	 */
	private void initialiseThreads(){
		for(Integer i = 0; i < MAX_THREADS; i++) {
			workers[i] = new Worker(queue, i.toString());
			threads[i] = new Thread(workers[i]);
			threads[i].start();
		}
	}
	
	public void runTask(Runnable r) {
		queue.enqueue(r);
	}
	
	public synchronized void stopThreadPool() {
		for(Integer i = 0; i < MAX_THREADS; i++) {
			workers[i].stop();
		}
		System.exit(0);
	}
	
	/**
	 * Control page and states for each thread
	 * @return
	 */
	public synchronized boolean workComplete() {
		boolean flag = false;
		String requestTemp = "N/A";
		for(Integer i = 0; i < MAX_THREADS; i++) {
			try {
				if(threads[i].getState().name().equals("RUNNABLE")) {
					flag = true;
				}	
			}
			catch(Exception e) {
				
			}
		}
		return flag ? false : true;
	}
}
