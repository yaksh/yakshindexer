package edu.upenn.cis455.postProccessing;

public interface BlockingQueue<E> {
	public void enqueue(E e);
	public E dequeue();
}