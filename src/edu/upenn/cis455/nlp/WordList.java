package edu.upenn.cis455.nlp;

import java.util.ArrayList;

public class WordList {
	private String word;
	private double count;
	private double sentenceCount;
	private double weight; 
	private ArrayList sentencePositionList;
	private ArrayList wordPositionList;
	private ArrayList wordLocationList;
 
	public WordList(String wd) {
		this.word = new String(wd);
		this.sentencePositionList = new ArrayList();
		this.wordPositionList = new ArrayList();
		this.wordLocationList = new ArrayList();
		this.count = 0;
		this.sentenceCount= 0;
	}  

	public void incrementCount(int sentencePosition, int wordPosition, int wordLocation) {
		this.count++;
		setSentensePosition(sentencePosition);
		setWordPosition(wordPosition);
		setWordLocation(wordLocation);
	} 

	public double getcount() {   
		return this.count; 
	}
	
	public String getword() {   
		return this.word; 
	}
	
	public void setSentensePosition(int sentencePosition) { 
		if(!sentencePositionList.contains(sentencePosition+""))
			this.sentenceCount++;
		sentencePositionList.add(sentencePosition+"");  
	}

	public void setWordPosition(int wordPosition) { 
		wordPositionList.add(wordPosition+"");  
	}
	
	public void setWordLocation(int wordLocation) { 
		wordLocationList.add(wordLocation+"");  
	}
	
	public ArrayList getWordPosition() { 
		return this.wordPositionList; 
	}
	
	public ArrayList getSentensePosition() { 
		return this.sentencePositionList; 
	}
	
	public ArrayList getWordLocation() { 
		return this.wordLocationList; 
	}
	
	public void setWeight(double weight) { 
		this.weight = weight; 
	}
	
	public double getWeight() { 
		return this.weight; 
	}
	
	public double getSentenseCount() { 
		return this.sentenceCount; 
	}
 
}