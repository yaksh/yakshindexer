package edu.upenn.cis455.nlp;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.StringTokenizer;

public class Stopword {       
   
	String[] stopWords;

	public Stopword(){
		stopWords = StopwordFactory.getStopwords();	        	
	}

	public void display () {
		for (int i=0;i<stopWords.length;i++)
			System.out.println(stopWords[i]);	
	}

	public boolean isStopword(String word) {
		boolean flag=false;        
		for (int i =0; i<stopWords.length; i++)  {
			if(stopWords[i].equalsIgnoreCase(word)) {
				flag = true;
				break;	
			}
		}
		return flag;	
	}

	public String[] getTokens(String sen) {
		
		int sz = 0, cnt=0;
		String words[] = null;
		StringTokenizer tokenizer = new StringTokenizer(sen) ;
		sz = tokenizer.countTokens();
		words=new String[sz];	        	
		while (tokenizer.hasMoreTokens()) {
           	words[cnt] = new String(tokenizer.nextToken());	     				
           	cnt++;
       	}		
		return words;	        
	}
  
	public String remove(String sen) {
		String dsen="";
		String[] words = getTokens(sen);	
		for (int j=0; j<words.length; j++) { 	
			if (!isStopword(words[j] )  )  	
				dsen = dsen + words[j] + " ";              		
		}		
		return dsen;	         		
    }    

	public static void main(String str[]) {
//		Stopword sp = new Stopword();	
//        System.out.println(sp.remove("a hello you are mine of about at are  murugan kannan"));
	}
   
}