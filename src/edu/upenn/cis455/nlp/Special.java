package edu.upenn.cis455.nlp;

import java.io.*;
import java.util.StringTokenizer;

public class Special {
	
	char[] specialChars = new char[]{',','\"',';','!','&','/','$',':','|','%',
									')','(','{','}','[',']','\'','.','-','_',
									'*','@','~','^','<','>','`','=','\\'};

	public Special() {
	        	
	}


	public char[] getTokens(String sen) {
		int sz=0,cnt=0;char words[]=null;
		StringTokenizer  stk=new StringTokenizer(sen);
		sz=stk.countTokens();
     	words=new char[sz];	 
     	
     	while ( stk.hasMoreTokens()) {
           	words[cnt]=new String(stk.nextToken()).charAt(0);	     				
           	cnt++;
       	}		
     	return words;	        
    }
  
	public String remove(String sen) {   
        return sen.replaceAll("[^a-zA-Z. ]", " ").replaceAll(" +", " ");
    }    

	public static void main(String str[]) {
		Special sp = new Special();	
		System.out.println(sp.remove("alt='chocolate' vanilla-chocolate adam_chocolate a hello you are mine! of% about  at  are ( murugan )kannan."));	
	}
   
}