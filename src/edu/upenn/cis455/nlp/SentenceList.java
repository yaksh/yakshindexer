package edu.upenn.cis455.nlp;

public class SentenceList implements Comparable {
	
	String rawSentence = "", stopRemovedSentence = "";
	private double weight;
	
	public SentenceList(String sentence) {
		rawSentence = new String(sentence);
		weight = 0.0;	
	}

	@Override
	public int compareTo(Object arg0) {
		double otherWeight = ((SentenceList)arg0).getWeight();
		if(otherWeight > this.getWeight()){
			return 1;
		}
		else if(otherWeight < this.getWeight()){
			return -1;
		}
		return 0;
	} 

	public void setRawSentense(String sentence) {
		rawSentence = new String(sentence);      	
	}
 
	public void setStopsRemovedSentence(String rawSentence) {
		stopRemovedSentence = new String(rawSentence);
	}  

	public String getRawSentence() {
		return rawSentence;
	}   
	
	public String getStopwordsRemovedSentence() {
		return stopRemovedSentence;
	}

	public void setWeight(double wg) {
		weight = weight + wg;
	} 
	
	public double getWeight() { 
		return weight; 
	}
}