package edu.upenn.cis455.job;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import edu.upenn.cis455.nlp.Summarizer;
import edu.upenn.cis455.nlp.Summary;
import edu.upenn.cis455.nlp.WordList;

public class IndexerMap extends Mapper<Text, Text, Text, Text> {
	
    private Text wordKey = new Text();
    private Text wordStats = new Text();
    private HashMap<String, ArrayList<Integer>> termCount;
    private Summarizer summarizer = null;
    private int summarizationLength = 10;

    @SuppressWarnings("unchecked")
	public void map(Text key, Text value, Context context) throws IOException, InterruptedException{
    	try{
//    		System.out.println(key.toString());
    		if(!(key.toString().toLowerCase().contains(".pdf")
					|| key.toString().toLowerCase().contains(".xps")
					|| key.toString().toLowerCase().contains(".jpeg")
					|| key.toString().toLowerCase().contains(".jpg")
					|| key.toString().toLowerCase().contains(".png")
					|| key.toString().toLowerCase().contains(".xml")
					|| key.toString().toLowerCase().contains(".mp4")
					|| key.toString().toLowerCase().contains(".zip")
					|| key.toString().toLowerCase().contains(".css")
					|| key.toString().toLowerCase().contains(".mp3")
					|| key.toString().toLowerCase().contains(".wma")
					|| key.toString().toLowerCase().contains(".gif")
					|| key.toString().toLowerCase().contains(".tar")
					|| key.toString().toLowerCase().contains(".gz")
					|| key.toString().toLowerCase().contains(".gzip"))){
    			termCount = new HashMap<>();
        		
        		//Preprocess the incoming text
        		summarizer = new Summarizer(value.toString());
        		Summary summary = summarizer.summarize(summarizationLength);	

//        		Enumeration<String> itr = summary.wordListMap.keys();
//                ArrayList<String> terms = new ArrayList<>();
                
//                String term = "";
//                ArrayList wordLocation = null;
//                while (itr.hasMoreElements()) {
//                	WordList wl = (WordList) summary.wordListMap.get(itr.nextElement());
//                	term = wl.getword();
//                	if(term.length() <= 15){
//                		wordLocation = wl.getWordLocation();
//                		
//                		if(wordLocation.size() > 0){
//                    		terms.add(wl.getword());
//                        	if(termCount.containsKey(term)){
//                        		ArrayList<Integer> termPos = termCount.get(term);
//                        		termPos.add(Integer.parseInt((String) wordLocation.remove(0)));
//                        		termCount.put(term, termPos);
//                        	}
//                        	else{
//                        		ArrayList<Integer> postions = new ArrayList<>();
//                        		postions.add(Integer.parseInt((String) wordLocation.remove(0)));
//                        		termCount.put(term, postions);
//                        	}
//                		}
//                	}
//                }
                
                for(String word : summary.wordListMap.keySet()){
                	WordList wl = (WordList) summary.wordListMap.get(word);
                	if(word.length() <= 15){
                    	StringBuffer valueUrls = new StringBuffer();
                    	ArrayList<String> postions = wl.getWordLocation();
                    	if(postions.size() > 0){
                    		valueUrls.append(key.toString().trim() + "{{");
                    	}
                    	for(String pos : postions){
                    		valueUrls.append(pos + ",");
                    	}
                    	wordKey.set(word);
                    	wordStats.set(valueUrls.substring(0, valueUrls.length() - 1));
                    	context.write(wordKey, wordStats);
                	}
                }
    		}
    	}
    	catch(Exception e){
    		System.out.println("Error in map phase for url " + key.toString());
    		e.printStackTrace();
    	}
    }
}
