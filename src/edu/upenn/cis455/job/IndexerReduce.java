package edu.upenn.cis455.job;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import edu.upenn.cis455.postProccessing.BTreeDB;

public class IndexerReduce extends Reducer <Text,Text,Text,Text> {
    
	private Text result = new Text();
	private BTreeDB db = new BTreeDB();

    public void reduce(Text key, Iterable<Text> values, Context context){
    	try{
            StringBuffer buffer = new StringBuffer();
            for (Text val : values) {
            	buffer.append(val.toString() + "||");
            }
            result.set(buffer.toString());
            db.addLine(key.toString() + "\t" + result.toString());
            context.write(key, result);
    	}
    	catch(Exception e){
    		System.out.println("Error in reduce phase for word " + key);
    	}
    }
}
