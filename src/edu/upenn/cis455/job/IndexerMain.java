package edu.upenn.cis455.job;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import edu.upenn.cis455.storage.S3Storage;

/**
 * @author Nikhilesh Behera
 * Main method for the job to start
 */
public class IndexerMain {
	
	public static String awsAccessKey;
	public static String awsSecretKey;
	public static String awsAccessKeyJatin;
	public static String awsSecretKeyJatin;
	public static String docsBucket = "search-documents";
	private static String s3LocationOutput = "s3n://yaksh-indexer/";
	
	public ArrayList<File> combinePageRankFiles(String inputDir) throws IOException{
		ArrayList<File> files = new ArrayList<>();
		//for all pageRank files put it into one file
		final String dir = System.getProperty("user.dir");
        System.out.println("current dir = " + dir);
		for(File f : new File(inputDir).listFiles()){
			if(f.getAbsolutePath().endsWith(".crc"))
				continue;
			if(f.getAbsolutePath().contains("_SUCCESS"))
				continue;
			System.out.println("[input files]" + f.getAbsolutePath());
			files.add(f);
		}
		return files;
	}	
	
	/**
	 * @param args
	 * Main method
	 */
	public static void main(String[] args) {
		try{
			//User input
			IndexerMain.awsAccessKey = args[0];
			IndexerMain.awsSecretKey = args[1];
			String outputFolder = IndexerMain.s3LocationOutput + args[2];
			IndexerMain.docsBucket = args[3];
			IndexerMain.awsAccessKeyJatin = args[4];
			IndexerMain.awsSecretKeyJatin = args[5];
			
//			String secret = IndexerMain.awsSecretKey.replaceAll("/", "%2F");
			
			//Connect to S3
			S3Storage s3 = new S3Storage(IndexerMain.awsAccessKey, IndexerMain.awsSecretKey);
			
			IndexerMain indexer = new IndexerMain();
			
			//Configure the indexing job
			Configuration conf = new Configuration();
//			conf.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());
			conf.set("hadoop.tmp.dir",System.getProperty("java.io.tmpdir"));
			conf.set("mapreduce.input.keyvaluelinerecordreader.key.value.separator", "\t");
			Job job = new Job(conf, "Indexer");
	        job.setMapperClass(IndexerMap.class);
	        job.setReducerClass(IndexerReduce.class);
	        job.setOutputKeyClass(Text.class);
	        job.setOutputValueClass(Text.class);
			job.setMapOutputKeyClass(Text.class);
			job.setMapOutputValueClass(Text.class);
	        job.setInputFormatClass(KeyValueTextInputFormat.class);
	        job.setOutputFormatClass(TextOutputFormat.class);
	        
	        ArrayList<String> list = s3.getFilesInFolder(IndexerMain.docsBucket, null);
			if(list.size() > 0){
				for(String file : list){
					FileInputFormat.addInputPath(job, new Path("s3n://" 
								+ IndexerMain.docsBucket + "/" +file));
				}
			}
	        FileOutputFormat.setOutputPath(job, new Path(outputFolder));
	        
//	        ArrayList<File> inputDir = indexer.combinePageRankFiles("/home/cis455/workspace/in");
//	        for(File f : inputDir){
//				FileInputFormat.addInputPath(job, new Path(f.getAbsolutePath()));
//			}
//	        FileOutputFormat.setOutputPath(job, new Path("outIndexer"));
	        job.setJarByClass(IndexerMain.class);
	        job.waitForCompletion(true);
		}
		catch(Exception e){
			System.err.println("Error in indexing job");
			e.printStackTrace();
		}
	}
}
