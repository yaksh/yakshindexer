package edu.upenn.cis455.job;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import edu.upenn.cis455.storage.S3Storage;

public class IndexerCleaner {
	
	public long TotalIndexCount = 0;
	public long TotalIndexDiscarded = 0;
	private static String awsAccessKey;
	private static String awsSecretKey;
	
	public void clean(String bucket, String folder){
		BufferedWriter writer = null;
		BufferedReader reader = null;
		try{
	    	S3Storage s3 = new S3Storage(IndexerCleaner.awsAccessKey, IndexerCleaner.awsSecretKey);
	    	ArrayList<String> outKeys = s3.getFilesInFolder(bucket, folder);
	    	File finalOutput = new File("/home/cis455/workspace/out/Indexer.csv");
	    	writer = new BufferedWriter(new FileWriter(finalOutput));
	    	for(String key : outKeys){
	    		try{
		    		File downloaded = File.createTempFile("index", ".csv");
		    		System.out.println("Downloading file " + key);
		    		s3.download(bucket, key, downloaded);
	    			reader = new BufferedReader(new FileReader(downloaded));
	    			String line = "";
	    			System.out.println("Proccessing file " + key);
	    			while((line = reader.readLine()) != null){
	    				String[] token = line.split("\t");
	    				if(token.length == 2){
		    				if(token[0].length() > 15
		    						|| token[1].toLowerCase().contains(".pdf")
		    						|| token[1].toLowerCase().contains(".xps")
		    						|| token[1].toLowerCase().contains(".jpeg")
		    						|| token[1].toLowerCase().contains(".jpg")
		    						|| token[1].toLowerCase().contains(".png")
		    						|| token[1].toLowerCase().contains(".xml")
		    						|| token[1].toLowerCase().contains(".mp4")
		    						|| token[1].toLowerCase().contains(".zip")){
		    					TotalIndexDiscarded++;
		    					continue;
		    				}
		    				else{
		    					writer.write(line);
		    					writer.newLine();
		    					TotalIndexCount++;
		    				}
	    				}
	    			}
	    		}
	    		catch(Exception e){
	    			System.err.println("Error processing file " + key);
	    			e.printStackTrace();
	    		}
	    		finally{
	    			if(reader != null)
	    				reader.close();
	    			System.out.println("Done Proccessing file " + key);
	    		}
	    	}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			if(writer != null){
				try {
					writer.close();
				} 
				catch (IOException e) {
					e.printStackTrace();
				}
			}
			System.out.println("Index count: " + TotalIndexCount + " Discarded: " + TotalIndexDiscarded);
		}
	}
	
	public void hist(String bucket, String folder){
		HashMap<Character, Long> histCount = new HashMap<>();
		BufferedReader reader = null;
		S3Storage s3 = new S3Storage(IndexerCleaner.awsAccessKey, IndexerCleaner.awsSecretKey);
    	ArrayList<String> outKeys = s3.getFilesInFolder(bucket, folder);
    	for(String key : outKeys){
    		try{
	    		File downloaded = File.createTempFile("index", ".csv");
	    		System.out.println("Downloading file " + key);
	    		s3.download(bucket, key, downloaded);
    			reader = new BufferedReader(new FileReader(downloaded));
    			String line = "";
    			System.out.println("Proccessing file " + key);
    			while((line = reader.readLine()) != null){
    				String[] token = line.split("\t");
    				if(token.length == 2){
    					char startChar = token[0].charAt(0);
    					if(!histCount.containsKey(startChar)){
    						long count = 1;
    						histCount.put(startChar, count);
    					}
    					else{
    						long counted = histCount.get(startChar);
    						histCount.put(startChar, (counted + 1));
    					}
    				}
    			}
    		}
    		catch(Exception e){
    			System.err.println("Error processing file " + key);
    			e.printStackTrace();
    		}
    		finally{
    			if(reader != null){
					try {
						reader.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
    			}
    			System.out.println("Done Proccessing file " + key);
    		}
    	}
    	for(Character ch : histCount.keySet()){
    		System.out.println(ch + " : " + histCount.get(ch));
    	}
	}
	
	public static void main(String[] args) {
		IndexerCleaner cleaner = new IndexerCleaner();
		IndexerCleaner.awsAccessKey = args[0];
		IndexerCleaner.awsSecretKey = args[1];
		cleaner.clean("yaksh-indexer", "out-8");
//		cleaner.hist("yaksh-indexer", "out-3");
		System.exit(0);
	}
}